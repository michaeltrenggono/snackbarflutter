import 'package:flutter/material.dart';

import 'dart:convert';

import './snackbar_item.dart';

class SnackBarList extends StatelessWidget {
  final Function toSnackBarForm;
  final List snacksList;

  SnackBarList(this.toSnackBarForm, this.snacksList);

  List showSnackBarList(List snacksList) {
    List<Widget> results = [];
    if(snacksList != null ) {
      snacksList.forEach((content) {
        Map snack = JsonDecoder().convert(content);
        print('snack image is');
        print(snack['img']);
        Widget snackBarItem = SnackBarItem(snack['name'], snack['qty'], snack['img']);
        results.add(snackBarItem);
      });
    }

    return results;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Start Adding Snack",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Column(
      children: <Widget>[
        ListView(
          shrinkWrap: true,
          children: showSnackBarList(snacksList),
        ),
        RaisedButton(
          onPressed: () {
            toSnackBarForm();
          },
          child: Text('Add a snack'),
        )
      ],
    ),
    );
  }
}
