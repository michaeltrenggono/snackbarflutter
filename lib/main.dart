import 'package:flutter/material.dart';

import './snackbar_manager.dart';

void main() => runApp(SnackBarApp());

class SnackBarApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'SnackBar Manager',
      theme: ThemeData(primarySwatch: Colors.lightBlue),
      home: SnackBarManager(),
    );
  }
}
