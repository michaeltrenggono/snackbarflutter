import 'package:flutter/material.dart';

import 'dart:io';

class SnackBarItem extends StatelessWidget {
  final snackName;
  final snackQty;
  final snackImage;

  SnackBarItem(this.snackName, this.snackQty, this.snackImage);

  Widget textSection(String text,
      {int flex = 1, TextAlign textAlign = TextAlign.left}) {
    return Expanded(
      child: Container(
        child: Text(
          text,
          style: TextStyle(fontSize: 24),
          textAlign: textAlign,
        ),
        padding: EdgeInsets.symmetric(horizontal: 10),
      ),
      flex: flex,
    );
  }

  Widget imgGrid(String text, {int flex = 2, double height = 70.0}) {
    if (text != null) {
      if (text.startsWith('http')) {
        return Expanded(
          child: Image.network(
            text,
            height: height,
            fit: BoxFit.cover,
          ),
          flex: flex,
        );
      } else {
        return Expanded(
          child: Image.file(File(text)),
          flex: flex,
        );
      }
    } else {
      return Expanded(
        child: Image.network(
          'https://cdn1.medicalnewstoday.com/content/images/articles/308/308796/mixed-fruits.jpg',
          height: height,
          fit: BoxFit.cover,
        ),
        flex: flex,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          imgGrid(snackImage),
          textSection(snackName, flex: 6),
          textSection(snackQty, flex: 2, textAlign: TextAlign.right)
        ],
      ),
    );
  }
}
