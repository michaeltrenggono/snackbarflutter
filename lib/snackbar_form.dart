import 'package:flutter/material.dart';

class SnackBarForm extends StatelessWidget {
  final GlobalKey<FormState> _formKey;
  final snackNameController = TextEditingController();
  final snackQtyController =
      TextEditingController.fromValue(TextEditingValue(text: '1'));

  final Function addSnack;
  final Function addImageFromCamera;

  SnackBarForm(this._formKey, this.addSnack, this.addImageFromCamera);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Start Adding Snack",
          style: TextStyle(color: Colors.white),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.lightBlue))),
              child: TextFormField(
                decoration: InputDecoration(
                  hintText: 'Put your snack name here',
                  contentPadding: const EdgeInsets.all(7.5),
                ),
                autofocus: true,
                controller: snackNameController,
              ),
            ),
            Container(
              margin: const EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                  border: Border(bottom: BorderSide(color: Colors.lightBlue))),
              child: TextFormField(
                keyboardType: TextInputType.numberWithOptions(),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(7.5),
                ),
                controller: snackQtyController,
              ),
            ),
            RaisedButton(
                onPressed: () {
                  print('this button will open camera');
                  addImageFromCamera();
                },
                child: Text('Take Picture')),
            RaisedButton(
                onPressed: () {
                  addSnack(snackNameController.text, snackQtyController.text);
                  snackNameController.text = '';
                  snackQtyController.text = '1';
                  Navigator.pop(context);
                },
                child: Text('Add a snack'))
          ],
        ),
      ),
    );
  }
}
