import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class SnackBarManager extends StatefulWidget {
  // final List<String> snacksData = [];
  SnackBarManagerState createState() => SnackBarManagerState();
}

class SnackBarManagerState extends State<SnackBarManager> {
  List<String> snacksData = [];
  final snackNameController = TextEditingController();
  final snackQtyController = TextEditingController.fromValue(TextEditingValue(text: '1'));
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  String addSnackButton = 'Add a snack';

  @override
  void initState() {
    super.initState();
    _loadSnacks();
  }

  void _loadSnacks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
          snacksData = prefs.getStringList('snacksLocalData');
        });
    print(snacksData);
  }

  void _submit() async {
    print('submitting');
    if (_formKey.currentState.validate()) {
      if (snackNameController.text != '' &&
          int.parse(snackQtyController.text) > 0) {
        // saving data here
        SharedPreferences prefs = await SharedPreferences.getInstance();
        final Map snackMap = {
          'name': snackNameController.text,
          'qty': snackQtyController.text
        };
        String snackObjString = JsonEncoder().convert(snackMap);
        snackNameController.text = '';
        snackQtyController.text = '1';
        snacksData.add(snackObjString);

        setState(() {
          snacksData.add(snackObjString);
          prefs.setStringList('snacksLocalData', snacksData);
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "GMG's Snackbar Observer",
            style: TextStyle(color: Colors.white),
          ),
        ),
        body: Center(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Colors.lightBlue))),
                  child: TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Put your snack name here',
                      contentPadding: const EdgeInsets.all(7.5),
                    ),
                    autofocus: true,
                    controller: snackNameController,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                      border:
                          Border(bottom: BorderSide(color: Colors.lightBlue))),
                  child: TextFormField(
                    keyboardType: TextInputType.numberWithOptions(),
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(7.5),
                    ),
                    controller: snackQtyController,
                  ),
                ),
                RaisedButton(
                    onPressed: () {
                      _submit();
                    },
                    child: Text(addSnackButton))
              ],
            ),
          ),
        ));
  }
}
