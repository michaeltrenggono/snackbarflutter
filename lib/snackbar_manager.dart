import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';

import 'dart:io';
import 'dart:convert';

import './snackbar_login.dart';
import './snackbar_list.dart';
import './snackbar_form.dart';

class SnackBarManager extends StatefulWidget {
  SnackBarManagerState createState() => SnackBarManagerState();
}

class SnackBarManagerState extends State<SnackBarManager> {
  String imageFile;
  List<String> snacksList = [];
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  static const String WPAPIEP = 'http://192.168.1.4/clientportal/wp-admin/admin-ajax.php';

  @override
  void initState() {
    super.initState();
    _loadSnacks();
  }

  Future<Null> _addImageFromCamera() async {
    final imageFile = await ImagePicker.pickImage(source: ImageSource.camera);
    setState(() {
          this.imageFile = imageFile.path;
        });
  }

  Future _login(String username, String password) async {
    
    if(username != '' && password != '')
    { 
      http.post(WPAPIEP, body: {"action": 'app_login', "username": username, 'password': password}).then(
        (response) {
          final int statusCode = response.statusCode;

          if(statusCode < 200 || statusCode > 400 || json == null){
            throw new Exception("Error while fetching data");
          }
          
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SnackBarList(_toSnackBarForm, snacksList)),
          );
        }
      );
    }
    else
    {
      print('I forbid you to login!');
    }
  }

  void _loadSnacks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // prefs.remove('snacksLocalData');
    setState(() {
      snacksList = prefs.getStringList('snacksLocalData') == null ? [] : prefs.getStringList('snacksLocalData');
    });
  }

  void _addSnack(String snackName, String snackQty) async {
    if (_formKey.currentState.validate()) {
      if (snackName != '' && int.parse(snackQty) > 0) {
        print('adding');
        // saving data here
        SharedPreferences prefs = await SharedPreferences.getInstance();
        final Map snackMap = {'name': snackName, 'qty': snackQty, 'img': imageFile};
        String snackObjString = JsonEncoder().convert(snackMap);

        setState(() {
          snacksList.add(snackObjString);
          prefs.setStringList('snacksLocalData', snacksList);
        });


      }
    }
  }

  void _toSnackBarForm() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SnackBarForm(_formKey, _addSnack, _addImageFromCamera)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "GMG's Snackbar Observer",
          style: TextStyle(color: Colors.white),
        ),
      ),
      // body: SnackBarList(_toSnackBarForm, snacksList),
      body: SnackBarLogin(_login),
    );
  }
}
