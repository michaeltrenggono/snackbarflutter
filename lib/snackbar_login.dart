import 'package:flutter/material.dart';

class SnackBarLogin extends StatelessWidget {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();

  final Function login;

  SnackBarLogin(this.login);

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            decoration: InputDecoration(
              hintText: 'Username',
              contentPadding: const EdgeInsets.all(15),
            ),
            controller: usernameController,
          ),
          TextFormField(
            obscureText: true,
            decoration: InputDecoration(
              hintText: 'Password',
              contentPadding: const EdgeInsets.all(15),
            ),
            controller: passwordController,
          ),
          RaisedButton(
                onPressed: () {
                  login(usernameController.text, passwordController.text);
                },
                child: Text('Login'))
        ],
      ),
    ));
  }
}
